import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'
import Menuprincipal from '../views/Menuprincipal.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '../views/Menuprincipal',
      name: 'Menuprincipal',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: Menuprincipal
    },
    {
      path: "/login",
      name: "login",
      component: () => import("./components/Login")
    }
  ]

})
